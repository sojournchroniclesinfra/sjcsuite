<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Being extends Model
{
  protected $fillable = ['name', 'description', 'homeworld', 'born', 'died', 'species', 'gender', 'height', 'mass', 'hair', 'eyes', 'skin', 'eras', 'affiliation', 'creator_id', 'updator_id'];
  public function user()
  {
  return $this->belongsTo(User::class, 'creator_id');
  }
  public function updator()
  {
  return $this->belongsTo(User::class, 'updator_id');
  }
}
