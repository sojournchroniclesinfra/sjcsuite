<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galaxy extends Model
{
    protected $fillable = ['name', 'description', 'starcount', 'systemcount', 'size_ly', 'companion_galaxies', 'creator_id'];
    public function user()
    {
    return $this->belongsTo(User::class, 'creator_id');
    }
}
