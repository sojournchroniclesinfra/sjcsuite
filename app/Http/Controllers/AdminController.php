<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function index()
    {
      return view('admin.index');
    }

    public function userIndex()
    {
      $users = User::latest()->paginate(10);
      return view('admin.users.index', compact('users'));
    }
    public function userShow(User $user)
    {
      return view('admin.users.show', compact('user'));
    }

    public function createUser()
    {
      return view('admin.users.create');
    }

    public function userEdit(User $user)
    {
      return view('admin.users.edit', compact('user'));
    }
    public function userStore()
    {
      $this->validate(request(), [
        'name' => 'required',
        'email' => 'required'
      ]);
      User::create([
          'name' => request('name'),
          'email' => request('email'),
          'password' => bcrypt(request('password')),
      ]);
      return redirect('/admin/users');
    }
    public function userDestroy($id)
    {
      $user = User::find($id);
      $user->delete();
      return redirect('/admin/users');
    }
    public function adminToggle(Request $request, $id)
    {
      $user = User::find($id);
      $user->is_admin = $request->get('newStatus');
      if($request->get('newStatus') == 1) {
        $user->is_trusted = 1;
      }
      $user->save();
      return redirect('/admin/users/'.$id);
    }
    public function modToggle(Request $request, $id)
    {
      $user = User::find($id);
      $user->is_moderator = $request->get('newStatus');
      if($request->get('newStatus') == 1) {
        $user->is_trusted = 1;
      } 
      $user->save();
      return redirect('/admin/users/'.$id);
    }
    public function updateUser(Request $request, $id)
    {
      $user = User::find($id);
      $user->name = $request->get('name');
      $user->email = $request->get('email');
      $user->save();
      return redirect('/admin/users/'.$id);
    }
    public function trustToggle(Request $request, $id)
    {
      $user = User::find($id);
      $user->is_trusted = $request->get('newStatus');
      $user->save();
      return redirect('/admin/users/'.$id);
    }
    public function banToggle(Request $request, $id)
    {
      $user = User::find($id);
      $user->is_banned = $request->get('newStatus');
      $user->save();
      return redirect('/admin/users/'.$id);
    }

}
