<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class AtlasController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function index()
  {
    return view('atlas.index');
  }
  public function articleindex()
  {
    $articles = Article::latest()->paginate(10);
    return view('articles.index', compact('articles'));
  }
  public function entitycreate()
  {
    return view('atlas.createentity');
  }
  public function entitybrowse()
  {
    return view('atlas.entityindex');
  }
  public function show(Article $article)
  {
    return view('articles.show', compact('article'));
  }
  public function create()
  {
    return view('articles.create');
  }
  public function store()
  {

    $this->validate(request(), [
      'title' => 'required',
      'body' => 'required'
    ]);
    Article::create([
      'title' => request('title'),
      'body' => request('body'),
      'user_id' => auth()->user()->id
    ]);
    return redirect('/atlas');
  }
  public function destroy($id)
  {
    $article = Article::find($id);
    $article->delete();

    return redirect('/atlas/article');
  }
  public function update(Request $request, $id)
  {
    $article = Article::find($id);
    $article->title = $request->get('title');
    $article->body = $request->get('body');
    $article->save();
    return redirect('/atlas/article/'.$id);
  }
  public function edit(Article $article)
  {
    return view('articles.edit', compact('article'));
  }
}
