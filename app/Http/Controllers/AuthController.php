<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
  /**
   * Handle an authentication attempt.
   *
   * @return Response
   */
   public function authenticate(Request $request)
   {
      $credentials = array(
           'email' => $request->get('email'),
           'password' => $request->get('password'),
           'is_banned' => '0'
       );

       if (Auth::attempt($credentials))
       {
           return redirect()->intended('/');
       }
       else
       {
           return redirect()->intended('banned');
       }
   }
}
