<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Being;

class BeingController extends Controller
{
  public function index()
  {
    $beings = Being::latest()->paginate(10);
    return view('atlas.being.index', compact('beings'));
  }
  ## Display A single Being
  public function show(Being $being)
  {
    return view('atlas.being.show', compact('being'));
  }
  ## Create a new Being
  public function create()
  {
    return view('atlas.being.create');
  }
  public function store()
  {

    $this->validate(request(), [
      'name' => 'required',
      'description' => 'required'
    ]);
    Being::create([
      'name' => request('name'),
      'description' => request('description'),
      'homeworld' => request('homeworld'),
      'born' => request('born'),
      'died' => request('died'),
      'species' => request('species'),
      'gender' => request('gender'),
      'height' => request('height'),
      'mass' => request('mass'),
      'hair' => request('hair'),
      'eyes' => request('eyes'),
      'skin' => request('skin'),
      'eras' => request('eras'),
      'affiliation' => request('affiliation'),
      'creator_id' => auth()->user()->id
    ]);
    return redirect('/atlas/being');
  }
  public function edit(Being $being)
  {
    return view('atlas.being.edit', compact('being'));
  }
  public function update(Request $request, $id)
  {
    $being = Being::find($id);
    $being->name = $request->get('name');
    $being->description = $request->get('description');
    $being->homeworld = $request->get('homeworld');
    $being->born = $request->get('born');
    $being->died = $request->get('died');
    $being->species = $request->get('species');
    $being->gender = $request->get('gender');
    $being->height = $request->get('height');
    $being->mass = $request->get('mass');
    $being->hair = $request->get('hair');
    $being->eyes = $request->get('eyes');
    $being->skin = $request->get('skin');
    $being->eras = $request->get('eras');
    $being->affiliation = $request->get('affiliation');
    $being->updator_id = auth()->user()->id;
    $being->save();
    return redirect('/atlas/being/'.$id);
  }
  public function destroy($id)
  {
    $being = Being::find($id);
    $being->delete();

    return redirect('/atlas/being');
  }
  public function imageUpload(Request $request, $id)
  {
    $file = $request->file('imageFile');
    $file->storeAs('/public/atlasimages/being/', "{$id}.png");
    return back();
  }
}
