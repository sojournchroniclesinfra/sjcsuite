<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Galaxy;

class GalaxyController extends Controller
{
  # List All Galaxies
  public function index()
  {
    $galaxies = Galaxy::latest()->paginate(10);
    return view('atlas.galaxy.index', compact('galaxies'));
  }
  ## Display A single Galaxy
  public function show(Galaxy $galaxy)
  {
    return view('atlas.galaxy.show', compact('galaxy'));
  }
  ## Create a new Galaxy
  public function create()
  {
    return view('atlas.galaxy.create');
  }
  public function store()
  {

    $this->validate(request(), [
      'name' => 'required',
      'description' => 'required'
    ]);
    Galaxy::create([
      'name' => request('name'),
      'description' => request('description'),
      'starcount' => request('starcount'),
      'systemcount' => request('systemcount'),
      'size_ly' => request('size_ly'),
      'companion_galaxies' => request('companion_galaxies'),
      'creator_id' => auth()->user()->id
    ]);
    return redirect('/atlas/galaxy');
  }
  public function edit(Galaxy $galaxy)
  {
    return view('atlas.galaxy.edit', compact('galaxy'));
  }
  public function update(Request $request, $id)
  {
    $galaxy = Galaxy::find($id);
    $galaxy->name = $request->get('name');
    $galaxy->description = $request->get('description');
    $galaxy->starcount = $request->get('starcount');
    $galaxy->systemcount = $request->get('systemcount');
    $galaxy->size_ly = $request->get('size_ly');
    $galaxy->companion_galaxies = $request->get('companion_galaxies');
    $galaxy->updator_id = auth()->user()->id;
    $galaxy->save();
    return redirect('/atlas/galaxy/'.$id);
  }
  public function destroy($id)
  {
    $galaxy = Galaxy::find($id);
    $galaxy->delete();

    return redirect('/atlas/galaxy');
  }
  public function imageUpload(Request $request, $id)
  {
    $file = $request->file('imageFile');
    $file->storeAs('/public/atlasimages/galaxy/', "{$id}.png");
    return back();
  }
}
