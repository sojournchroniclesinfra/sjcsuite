<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MembershipInquiry;

class MembershipInquiryController extends Controller
{
  public function show($id)
  {
    $membershipInquiry = MembershipInquiry::find($id);
    return view('contribute.show', compact('membershipinquiry'));
  }
}
