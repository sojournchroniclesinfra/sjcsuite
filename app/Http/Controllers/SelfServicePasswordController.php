<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SelfServicePasswordController extends Controller
{
    public function index()
    {
      return view('user.passwordchange');
    }
    public function update(Request $request, $id)
    {
      $user = User::find($id);
      $user->password = bcrypt($request->get('password'));
      $user->save();
      return redirect('/');
    }
}
