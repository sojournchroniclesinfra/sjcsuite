<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;

class TicketController extends Controller
{
    public function index()
    {
      $tickets = Ticket::latest()->paginate(10);
    return view('tickets.index', compact('tickets'));
    }
}
