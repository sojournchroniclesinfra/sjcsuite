<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\World;

class WorldController extends Controller
{
  public function index()
  {
    $worlds = World::latest()->paginate(10);
    return view('atlas.world.index', compact('worlds'));
  }
  ## Display A single World
  public function show(World $world)
  {
    return view('atlas.world.show', compact('world'));
  }
  ## Create a new World
  public function create()
  {
    return view('atlas.world.create');
  }
  public function store()
  {

    $this->validate(request(), [
      'name' => 'required',
      'description' => 'required'
    ]);
    World::create([
      'name' => request('name'),
      'description' => request('description'),
      'region' => request('region'),
      'sector' => request('sector'),
      'system' => request('system'),
      'suns' => request('suns'),
      'orbital_pos' => request('orbital_pos'),
      'moons' => request('moons'),
      'rotation_period' => request('rotation_period'),
      'orbital_period' => request('orbital_period'),
      'class' => request('class'),
      'diameter' => request('diameter'),
      'atmosphere' => request('atmosphere'),
      'gravity' => request('gravity'),
      'terrain' => request('terrain'),
      'surface_water' => request('surface_water'),
      'poi' => request('poi'),
      'flora' => request('flora'),
      'fauna' => request('fauna'),
      'native_species' => request('native_species'),
      'immigrated_species' => request('immigrated_species'),
      'languages' => request('languages'),
      'government' => request('government'),
      'population' => request('population'),
      'cities' => request('cities'),
      'imports' => request('imports'),
      'exports' => request('exports'),
      'affiliation' => request('affiliation'),
      'creator_id' => auth()->user()->id
    ]);
    return redirect('/atlas/world/');
  }
  public function edit(World $world)
  {
    return view('atlas.world.edit', compact('world'));
  }
  public function update(Request $request, $id)
  {
    $world = World::find($id);
    $world->name = $request->get('name');
    $world->description = $request->get('description');
    $world->sector = $request->get('sector');
    $world->region = $request->get('region');
    $world->system = $request->get('system');
    $world->suns = $request->get('suns');
    $world->orbital_pos = $request->get('orbital_pos');
    $world->moons = $request->get('moons');
    $world->rotation_period = $request->get('rotation_period');
    $world->orbital_period = $request->get('orbital_period');
    $world->class = $request->get('class');
    $world->diameter = $request->get('diameter');
    $world->atmosphere = $request->get('atmosphere');
    $world->gravity = $request->get('gravity');
    $world->terrain = $request->get('terrain');
    $world->surface_water = $request->get('surface_water');
    $world->poi = $request->get('poi');
    $world->flora = $request->get('flora');
    $world->fauna = $request->get('fauna');
    $world->native_species = $request->get('native_species');
    $world->immigrated_species = $request->get('immigrated_species');
    $world->languages = $request->get('languages');
    $world->government = $request->get('government');
    $world->population = $request->get('population');
    $world->cities = $request->get('cities');
    $world->imports = $request->get('imports');
    $world->exports = $request->get('exports');
    $world->affiliation = $request->get('affiliation');
    $world->save();
    return redirect('/atlas/world/'.$id);
  }
  public function destroy($id)
  {
    $world = World::find($id);
    $world->delete();

    return redirect('/atlas/world');
  }
  public function imageUpload(Request $request, $id)
  {
    $file = $request->file('imageFile');
    $file->storeAs('/public/atlasimages/world/', "{$id}.png");
    return back();
  }
}
