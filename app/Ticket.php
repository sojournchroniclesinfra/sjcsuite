<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
  protected $fillable = ['project', 'creator', 'summary', 'description', 'assignee', 'status', 'resolution'];
  public function creator()
  {
  return $this->belongsTo(User::class);
  }
  public function assignee()
  {
  return $this->belongsTo(User::class);
  }
}
