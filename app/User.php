<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'is_moderator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
      return $this->hasMany(Article::class);
    }
    public function createdTickets()
    {
      return $this->hasMany(Ticket::class);
    }
    public function assignedTickets()
    {
      return $this->hasMany(Ticket::class);
    }
    public function entities()
    {
      return $this->hasMany(Galaxy::class);
      return $this->hasMany(Being::class);
    }
}
