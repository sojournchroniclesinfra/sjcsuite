<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class World extends Model
{
  protected $fillable = ['name', 'description', 'region', 'sector', 'system', 'suns', 'orbital_pos', 'moons', 'rotation_period', 'orbital_period', 'class', 'diameter', 'atmosphere', 'gravity', 'terrain', 'surface_water', 'poi', 'flora', 'fauna', 'native_species', 'immigrated_species', 'languages', 'government', 'population', 'cities', 'imports', 'exports', 'affiliation', 'creator_id'];
  public function user()
  {
  return $this->belongsTo(User::class, 'creator_id');
  }
  public function updator()
  {
  return $this->belongsTo(User::class, 'updator_id');
  }
}
