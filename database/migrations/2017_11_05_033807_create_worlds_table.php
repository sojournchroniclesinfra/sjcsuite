<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worlds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('region');
            $table->string('sector');
            $table->string('system');
            $table->string('suns');
            $table->string('orbital_pos');
            $table->string('moons');
            $table->string('rotation_period');
            $table->string('orbital_period');
            $table->string('class');
            $table->integer('diameter');
            $table->string('atmosphere');
            $table->string('gravity');
            $table->string('terrain');
            $table->string('surface_water');
            $table->string('poi');
            $table->text('flora');
            $table->text('fauna');
            $table->text('native_species');
            $table->text('immigrated_species');
            $table->text('languages');
            $table->string('government');
            $table->integer('population');
            $table->text('cities');
            $table->text('imports');
            $table->text('exports');
            $table->text('affiliation');
            $table->longText('description');
            $table->integer('creator_id');
            $table->integer('updator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worlds');
    }
}
