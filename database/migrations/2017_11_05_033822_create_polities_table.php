<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->string('constitution');
            $table->string('leader');
            $table->string('cinc');
            $table->string('executive');
            $table->string('legislative');
            $table->string('judicial');
            $table->string('military');
            $table->string('capital');
            $table->string('currency');
            $table->string('religious_body');
            $table->string('holiday');
            $table->string('established');
            $table->string('reorganized');
            $table->string('dissolved');
            $table->string('restored');
            $table->longText('description');
            $table->integer('creator_id');
            $table->integer('updator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polities');
    }
}
