<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('homeworld');
            $table->string('born');
            $table->string('died');
            $table->string('species');
            $table->string('gender');
            $table->integer('height');
            $table->integer('mass');
            $table->string('hair');
            $table->string('eyes');
            $table->string('skin');
            $table->text('eras');
            $table->text('affiliation');
            $table->longText('description');
            $table->integer('creator_id');
            $table->integer('updator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beings');
    }
}
