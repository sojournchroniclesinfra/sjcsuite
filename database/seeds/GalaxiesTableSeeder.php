<?php

use Illuminate\Database\Seeder;

class GalaxiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker\Factory::create();
      DB::table('galaxies')->insert([
      'name' => $faker->name,
      'starcount' => rand(),
      'systemcount' => rand(),
      'size_ly' => rand(),
      'companion_galaxies' => str_random(10),
      'creator_id' => 1,
      'created_at' => $faker->dateTimeThisMonth,
      'description' => $faker->text
]);
    }
}
