<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../favicon.ico">
  <!-- Start Stylesheets -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
  <link href="/css/open-iconic-bootstrap.css" rel="stylesheet">
  <link href="/css/main.css" rel=stylesheet>
  <!-- End Stylesheets -->
  <!-- Start Jquery -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  <!-- End Jquery -->
  <title>Access Denied</title>
</head>
<body>
  @include('partials.guestnav')
  <div class="container">
<div class="jumbotron">
  <center><h1><span class="oi oi-ban"></span> 403 Forbidden</h1></center>
  <center><h4>Not So Fast.</h4></center>
  <center><p class="lead">You don't have permission to access that...<em><span id="display-domain"></span></em></p></center>
  <center><a href="/" class="btn btn-primary">Go Home</a></center>
</div>
</div>
<div class="container">
  <div class="body-content">
    <div class="row">
      <div class="col-md-6">
        <h2>What happened?</h2>
        <p class="lead">A 403 error indicates that you don't have permission to access the file or page you attempted to navigate to.</p>
      </div>
      <div class="col-md-6">
      <img src="https://i.imgur.com/kgfV66r.gif" alt="Dennis Nedry Finger-wag">
     </div>
    </div>
  </div>
</div>
</body>
</html>
