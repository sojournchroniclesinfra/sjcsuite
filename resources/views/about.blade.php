@extends('layouts.master')
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <div class="container-fluid">
            <h1>Sojourn Chronicles</h1>
            <p>A Science Fiction <a href="https://www.reddit.com/r/worldbuilding/wiki/index#wiki_frequently_asked_questions">worldbuilding</a> project.</p>
          </div>
        </div>
        <div class="container-fluid">
          <h2>About the Project</h2>
          <p>The Sojourn Chronicles is a worldbuilding project revolving around 1700 worlds connected by a network of portals.</p>
          <ul>
            <li>Genre: Science Fiction / Fantasy</li>
            <li>Setting: The Nexus Realms - A series of worlds connected by portals</li>
          </ul>
          <p>The setting contains 1700 worlds, which are all connected via a network of portals.</p>
          <p>In the early eras, space travel was limited, expensive, and slow. The worlds were scattered throughout the galaxy. Worlds that were directly linked may be decades away in terms of space travel.</p>
          <p>There was one location, a construct in space called the Nexus. All worlds in the Nexus Realms linked to the Nexus through artificially constructed portals. Some naturally occurring portals existed that directly linked worlds, however for the most part these were unstable and short lived.</p>
          <hr>
          <a href="../" class="btn btn-dark">Return</a>
        </div>
      </div>
    </div>
  </div>
@endsection
