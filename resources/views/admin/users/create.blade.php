@extends('layouts.adminmaster')
@section('content')
<div class="col-md-12">
  <h1>Register a User</h1>
  <hr>
<div class="row">
  <div class="col-md-4">
    <h3>User Information</h3>
    <form method="POST" action="/admin/users">
      {{ csrf_field() }}
      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter users's full name" name="name" required autofocus value="{{ old('name') }}">
        <small id="nameHelp" class="form-text text-muted">Enter the user's full name.</small>
      </div>
      <div class="form-group">
        <label for="email">Email Address</label>
        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email." name="email" required>
        <small id="emailHelp" class="form-text text-muted">Enter the user's email.</small>
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" aria-describedby="passwordHelp" placeholder="Enter password." name="password">
        <small id="passwordHelp" class="form-text text-muted">Enter a temporary password for the user to use on first login.</small>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
</div>
@endsection
