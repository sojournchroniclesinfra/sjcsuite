@extends('layouts.adminmaster')
@php
$id = Auth::user()->name;
@endphp
@section('content')
<div class="col-md-12">
  <h1>Editing {{ $user->name }}</h1>
  <hr>
<div class="row">
  <div class="col-md-4">
    <h3>User Information</h3>
    <form method="POST" action="{{action('AdminController@updateUser', $id)}}">
      {{ csrf_field() }}
      <input name="_method" type="hidden" value="PATCH">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter users's full name" name="name" required autofocus value="{{ $user->name }}">
        <small id="nameHelp" class="form-text text-muted">Enter the user's full name.</small>
      </div>
      <div class="form-group">
        <label for="email">Email Address</label>
        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email." name="email" required value="{{ $user->email }}">
        <small id="emailHelp" class="form-text text-muted">Enter the user's email.</small>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
</div>
@endsection
