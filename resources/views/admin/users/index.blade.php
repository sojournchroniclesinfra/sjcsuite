@extends('layouts.adminmaster')
@section('title')
  User Management
@endsection
@section ('content')
@php
  $okCheck = '<i class="fa fa-check-circle" aria-hidden="true" style="color: #218838;"></i>';
  $noCheck = '<i class="fa fa-times-circle" aria-hidden="true" style="color: #c82333;"></i>';
@endphp
  <div class="col-md-12">
    <h2>Users</h2>
    <hr>
    <table class="table table-striped">
    <thead>
      <tr>
        <th>Details</th>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th>Admin</th>
        <th>Moderator</th>
        <th>Trusted</th>
        <th>Banned</th>
      </tr>
    </thead>
  <tbody>
    @foreach ($users as $user)
      @include('admin.users.user')
    @endforeach
    {{-- <nav class="blog-pagination">
      <a class="btn btn-outline-primary" href="#">Older</a>
      <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
    </nav> --}}
  </tbody>
  </table>
  {{ $users->links() }}
  <a href="/admin/users/create" class="btn btn-primary"><span class="oi oi-plus"></span> Create New</a>
  </div>

@endsection
@section ('footer')
