@extends('layouts.adminmaster')
@section('title')
  {{ $user->name }}
@endsection
@section('content')
@php
  $okCheck = '<i class="fa fa-check-circle" aria-hidden="true" style="color: #218838;"></i>';
  $noCheck = '<i class="fa fa-times-circle" aria-hidden="true" style="color: #c82333;"></i>';
@endphp
<div class="col-md-12">
  <form action="{{action('AdminController@userDestroy', $user->id)}}" method="post">
    {{csrf_field()}}
    <input name="_method" type="hidden" value="DELETE">
    <button style="float:right;"class="btn btn-outline-danger btn-sm" type="submit">Delete</button>
  </form>
  <h1>
    {{ $user->name }}
  </h1>
  <small class="text-muted"><b><span class="oi oi-pencil"></span> <a href="/admin/users/{{ $user->id }}/edit">Edit this user</a></b></small>

  <hr>
  <div class="row">
    <div class="col-md-12">
      <h3>General User Data</h3>
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Key</th>
            <th scope="col">Value</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>User ID</td>
            <td>{{ $user->id }}</td>
          </tr>
          <tr>
            <td>Name</td>
            <td>{{ $user->name }}</td>
          </tr>
          <tr>
            <td>Email</td>
            <td>{{ $user->email }}</td>
          </tr>
          <tr>
            <td>Created At</td>
            <td>{{ $user->created_at }}</td>
          </tr>
          <tr>
            <td>Updated At</td>
            <td>{{ $user->updated_at }}</td>
          </tr>
        </tbody>
      </table>
      <hr>
      <h3>Privileges</h3>
      @if ($user->is_admin == 1)
        @php
          $admin = $okCheck;
        @endphp
      @else
        @php
          $admin = $noCheck;
        @endphp
      @endif
      @if ($user->is_moderator == 1)
        @php
          $moderator = $okCheck;
        @endphp
      @else
        @php
          $moderator = $noCheck;
        @endphp
      @endif
      @if ($user->is_trusted == 1)
        @php
          $trusted = $okCheck;
        @endphp
      @else
        @php
          $trusted = $noCheck;
        @endphp
      @endif
      @if ($user->is_banned == 1)
        @php
          $banned = '<i class="fa fa-exclamation-circle" aria-hidden="true" style="color: #c82333;"></i>';
        @endphp
      @else
        @php
          $banned = '<i class="fa fa-circle-thin" aria-hidden="true" style="color: #218838;"></i>';
        @endphp
      @endif
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Permission/Role</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Admin {!! $admin !!}</td>
            <td>
            @if ($user->is_admin == 0)
            <form id ="adminToggleForm" action="{{action('AdminController@adminToggle', $user->id)}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="PATCH">
              <input name="newStatus" type="hidden" value="1">
              <button id="adminToggle" type="button" class="btn btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                <div class="handle"></div>
              </button>
            </form>
            @endif
            @if ($user->is_admin == 1)
            <form id ="adminToggleForm" action="{{action('AdminController@adminToggle', $user->id)}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="PATCH">
              <input name="newStatus" type="hidden" value="0">
              <button id="adminToggle" type="button" class="btn btn-toggle active" data-toggle="button" aria-pressed="true" autocomplete="off">
                <div class="handle"></div>
              </button>
            </form>
            @endif
            <script type="text/javascript">
              $(function(){
               $('#adminToggle').on('click',function(){
                  $('#adminToggleForm').submit();
                  });
              });
            </script>
            </td>
            </td>
          </tr>
          <tr>
            <td>Moderator {!! $moderator !!}</td>
            <td>
            @if ($user->is_moderator == 0)
            <form id ="modToggleForm" action="{{action('AdminController@modToggle', $user->id)}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="PATCH">
              <input name="newStatus" type="hidden" value="1">
              <button id="modToggle" type="button" class="btn btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                <div class="handle"></div>
              </button>
            </form>
            @endif
            @if ($user->is_moderator == 1)
            <form id ="modToggleForm" action="{{action('AdminController@modToggle', $user->id)}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="PATCH">
              <input name="newStatus" type="hidden" value="0">
              <button id="modToggle" type="button" class="btn btn-toggle active" data-toggle="button" aria-pressed="true" autocomplete="off">
                <div class="handle"></div>
              </button>
            </form>
            @endif
            <script type="text/javascript">
              $(function(){
               $('#modToggle').on('click',function(){
                  $('#modToggleForm').submit();
                  });
              });
            </script>
            </td>
            </td>
          </tr>
          <tr>
            <td>Trusted {!! $trusted !!}</td>
            <td>
            @if ($user->is_trusted == 0)
            <form id ="trustToggleForm" action="{{action('AdminController@trustToggle', $user->id)}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="PATCH">
              <input name="newStatus" type="hidden" value="1">
              <button id="trustToggle" type="button" class="btn btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                <div class="handle"></div>
              </button>
            </form>
            @endif
            @if ($user->is_trusted == 1)
            <form id ="trustToggleForm" action="{{action('AdminController@trustToggle', $user->id)}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="PATCH">
              <input name="newStatus" type="hidden" value="0">
              <button id="trustToggle" type="button" class="btn btn-toggle active" data-toggle="button" aria-pressed="true" autocomplete="off">
                <div class="handle"></div>
              </button>
            </form>
            @endif
            <script type="text/javascript">
              $(function(){
               $('#trustToggle').on('click',function(){
                  $('#trustToggleForm').submit();
                  });
              });
            </script>
            </td>
            </td>
          </tr>
          <tr>
            @if ($user->is_banned == 0)
              <td>Banned {!! $banned !!}</td>
            @else
              <td><span style="color: #c82333">Banned</span> {!! $banned !!}</td>
            @endif

            <td>
            @if ($user->is_banned == 0)
            <form id ="banToggleForm" action="{{action('AdminController@banToggle', $user->id)}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="PATCH">
              <input name="newStatus" type="hidden" value="1">
              <button id="banToggle" type="button" class="btn btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                <div class="handle"></div>
              </button>
            </form>
            @endif
            @if ($user->is_banned == 1)
            <form id ="banToggleForm" action="{{action('AdminController@banToggle', $user->id)}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="PATCH">
              <input name="newStatus" type="hidden" value="0">
              <button id="banToggle" type="button" class="btn btn-toggle active" data-toggle="button" aria-pressed="true" autocomplete="off">
                <div class="handle"></div>
              </button>
            </form>
            @endif
            <script type="text/javascript">
              $(function(){
               $('#banToggle').on('click',function(){
                  $('#banToggleForm').submit();
                  });
              });
            </script>
            </td>
            </td>
          </tr>
        </tbody>
      </table>
      <hr>
        <div class="alert alert-info">If this user requires a password reset, advise them to visit the <a href="/password/reset">forgot password</a> page.</div>
      <hr>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>
@endsection
