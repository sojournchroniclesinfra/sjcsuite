@php
  if ($user->is_admin == 1) {
    $adminStatus = $okCheck;
  } else {
    $adminStatus = $noCheck;
  }
  if ($user->is_moderator == 1) {
    $modStatus = $okCheck;
  } else {
    $modStatus = $noCheck;
  }
  if ($user->is_trusted == 1) {
    $trustStatus = $okCheck;
  } else {
    $trustStatus = $noCheck;
  }
  if ($user->is_banned == 1) {
    $banStatus = '<i class="fa fa-exclamation-circle" aria-hidden="true" style="color: #c82333;"></i>';
  } else {
    $banStatus = '<i class="fa fa-circle-thin" aria-hidden="true" style="color: #218838;"></i>';
  }
@endphp






<tr>
  <td><a href="/admin/users/{{ $user->id }}" class="btn btn-primary"><span class="oi oi-magnifying-glass"></span></a></td>
  <td>{{ $user->id }}</td>
  <td>{{ $user->name }}</td>
  <td>{{ $user->email }}</td>
  <td>{{ $user->created_at }}</td>
  <td>{{ $user->updated_at }}</td>
  <td>{!! $adminStatus !!}</td>
  <td>{!! $modStatus !!}</td>
  <td>{!! $trustStatus !!}</td>
  <td>{!! $banStatus !!}</td>
</tr>
