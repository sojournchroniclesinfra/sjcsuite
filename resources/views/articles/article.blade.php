<div class="blog-post">
  <h2 class="blog-post-title">
    <a href="/atlas/article/{{ $article->id }}">
      {{ $article->title }}
    </a>
  </h2>
  @if(Auth::user()->is_moderator == TRUE)
  <form id="articleForm{{ $article->id }}" action="{{action('AtlasController@destroy', $article->id)}}" method="post">
    {{csrf_field()}}
    <input name="_method" type="hidden" value="DELETE">
    <button class="btn btn-outline-danger btn-sm delete-button" type="button">Delete</button>
    {{-- <a href="javascript:{}" class="entity_delete" onclick="form.submit();">Delete</a> --}}
  </form>
  @endif
  <small class="text-muted">
    <p class="blog-post-meta">
      {{ $article->user->name }} on
      {{ $article->created_at->toFormattedDateString() }}
    </p>
  </small>
  @php
    $shortened_body = str_limit($article->body,250);
    $markdown_body = parsedown($shortened_body);
  @endphp
{{ strip_tags($markdown_body, '<p>') }}
</div>
<script>
    $('.delete-button').on('click', function(e) {
        e.preventDefault();
        f=$(this).closest('form');
        swal({
            title: 'Are you sure?',
            text: "This can't be undone.",
            icon: 'warning',
            dangerMode: true,
            buttons: {
              cancel: {
                text: "Cancel",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
              },
              confirm: {
                text: "Delete",
                value: true,
                visible: true,
                className: "",
                closeModal: true
              }
            }
        })
        .then((willDelete) => {
          if (willDelete) {

            swal({
              title: "Article Deleted",
              text: "The selected article has now been deleted.",
              icon: "success",
              buttons: false,
            });
            setTimeout(function() {
            f.submit();
          }, 1000);
          }});
      });
</script>
