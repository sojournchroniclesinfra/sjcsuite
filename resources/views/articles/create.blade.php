@extends('layouts.atlasmaster')
@section('title')
  Create Article
@endsection
@section('content')
<div class="col-sm-8 blog-main">
  <h1>Create an article</h1>
  <hr/>
  <form method="POST" action="/atlas/article">
    {{ csrf_field() }}
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter title." name="title">
    <small id="titleHelp" class="form-text text-muted">Enter the title of the article.</small>
  </div>
  <div class="form-group">
    <label for="body">Body</label>
    <textarea class="form-control" name="body" id="body" aria-describedby="bodyHelp" rows="8" cols="80"></textarea>
    <small id="bodyHelp" class="form-text text-muted">Write the primary body/text of the article.</small>
  </div>
<div class="form-group">
  <a href="/atlas/article" class="btn btn-light">Return</a>
  <button type="submit" class="btn btn-primary">Publish</button>
</div>
@include('layouts.errors')
</form>
<script>
var simplemde = new SimpleMDE({ element: document.getElementById("body") });
</script>
</div>
@endsection
