@extends('layouts.atlasmaster')
@section('title')
  Editing "{{ $article->title }}"
@endsection
@section('content')
<div class="col-sm-8 blog-main">
  <h1>Editing "{{ $article->title }}"</h1>
  <hr/>
  <form method="POST" action="{{action('AtlasController@update', $article->id)}}">
    {{ csrf_field() }}
    <input name="_method" type="hidden" value="PATCH">
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter title." name="title" value="{{ $article->title }}">
    <small id="titleHelp" class="form-text text-muted">Enter the title of the article.</small>
  </div>
  <div class="form-group">
    <label for="body">Body</label>
    <textarea class="form-control" name="body" id="body" aria-describedby="bodyHelp" rows="8" cols="80">{{ $article->body }}</textarea>
    <small id="bodyHelp" class="form-text text-muted">Write the primary body/text of the article.</small>
  </div>
<div class="form-group">
  <a href="/atlas/article/{{ $article->id }}" class="btn btn-light">Return</a>
  <button type="submit" class="btn btn-primary">Publish</button>
</div>
@include('layouts.errors')
</form>
<script>
var simplemde = new SimpleMDE({ element: document.getElementById("body") });
</script>
</div>
@endsection
