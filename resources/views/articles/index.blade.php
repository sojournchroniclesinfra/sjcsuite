@extends('layouts.atlasmaster')
@section('title')
  Articles  
@endsection
@section ('content')
      <div class="col-sm-8 blog-main">
        @foreach ($articles as $article)
          @include('articles.article')
          <hr>
        @endforeach
        {{-- <nav class="blog-pagination">
          <a class="btn btn-outline-primary" href="#">Older</a>
          <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
        </nav> --}}

      </div>
      {{ $articles->links() }}

@endsection
@section ('footer')
@endsection
