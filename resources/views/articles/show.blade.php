@extends('layouts.atlasmaster')
@section('title')
  {{ $article->title }}
@endsection
@section('content')
  <div class="col-sm-8 blog-main">
    <h1>
      {{ $article->title }}
    </h1>
    <small class="text-muted"><i class="fa fa-pencil-square" aria-hidden="true"></i> <a href="/atlas/article/{{ $article->id }}/edit">Edit this article</a></small>
    <hr>
    {!! parsedown($article->body) !!}
    <hr>
  </div>
@endsection
