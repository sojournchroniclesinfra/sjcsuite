<div class="blog-post">
  <h2 class="blog-post-title">
    <a href="/atlas/being/{{ $being->id }}">
      {{ $being->name }}
    </a>
  </h2>
  @if(Auth::user()->is_moderator == TRUE)
  <form id="beingForm{{ $being->id }}" action="{{action('BeingController@destroy', $being->id)}}" method="post">
    {{csrf_field()}}
    <input name="_method" type="hidden" value="DELETE">
    <button class="btn btn-outline-danger btn-sm delete-button" type="button">Delete</button>
    {{-- <a href="javascript:{}" class="entity_delete" onclick="form.submit();">Delete</a> --}}
  </form>
  @endif
  <p class="blog-post-meta">
    <ul>
      <small class="text-muted"><li>Creator: {{ $being->user->name }}</li></small>
      <small class="text-muted"><li>Created: {{ $being->created_at->toFormattedDateString() }}</li></small>
    </ul>
  </p>
  {!! parsedown(str_limit($being->description,150)) !!}
</div>
<script>
    $('.delete-button').on('click', function(e) {
        e.preventDefault();
        f=$(this).closest('form');
        swal({
            title: 'Are you sure?',
            text: "You're about to wipe a being from the face of the universe.",
            icon: 'warning',
            dangerMode: true,
            buttons: {
              cancel: {
                text: "Cancel",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
              },
              confirm: {
                text: "Delete",
                value: true,
                visible: true,
                className: "",
                closeModal: true
              }
            }
        })
        .then((willDelete) => {
          if (willDelete) {

            swal({
              title: "Being Deleted",
              text: "Congratulations, you've basically committed murder.",
              icon: "success",
              buttons: false,
            });
            setTimeout(function() {
            f.submit();
          }, 1000);
          } else {
            swal({
              title: "Deletion Cancelled",
              text: "I'm sure that being is grateful.",
              icon: "info",
            });
          }
        });
      });
</script>
