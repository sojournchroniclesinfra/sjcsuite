@extends('layouts.atlasmaster')
@section('content')
<div class="col-md-12">
  <h1>Create a Being</h1>
  <hr>
<div class="row">
<div class="col-sm-8">
  <h3>Article Content</h3>
  <form method="POST" action="/atlas/being">
    {{ csrf_field() }}
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter name." name="name">
    <small id="nameHelp" class="form-text text-muted">Enter the name of the being.</small>
  </div>
  <div class="form-group">
    <label for="body">Description</label>
    <textarea class="form-control" name="description" id="description" aria-describedby="descriptionHelp" rows="8" cols="80"></textarea>
    <small id="descriptionHelp" class="form-text text-muted">Write a description/biography of the being.</small>
  </div>

<div class="form-group">
  <button type="submit" class="btn btn-primary">Publish</button>
</div>
<script>
var simplemde = new SimpleMDE({ element: document.getElementById("description") });
</script>
@include('layouts.errors')
</div>
<div class="col-sm-4">
  <h3>Entity Data</h3>
  <div class="form-group">
    <label for="homeworld">Homeworld</label>
    <input type="text" class="form-control" id="homeworld" aria-describedby="homeworldHelp" placeholder="Enter homeworld." name="homeworld">
    <small id="homeworldHelp" class="form-text text-muted">Enter this being's homeworld.</small>
  </div>
  <div class="form-group">
    <label for="born">Birthdate</label>
    <input type="text" class="form-control" id="born" aria-describedby="bornHelp" placeholder="Enter birthdate." name="born">
    <small id="bornHelp" class="form-text text-muted">Enter when this being was born.</small>
  </div>
  <div class="form-group">
    <label for="died">Dead date</label>
    <input type="text" class="form-control" id="died" aria-describedby="diedHelp" placeholder="Enter died." name="died">
    <small id="diedHelp" class="form-text text-muted">Enter when this being died.</small>
  </div>
  <div class="form-group">
    <label for="species">Species</label>
    <input type="text" class="form-control" id="species" aria-describedby="speciesHelp" placeholder="Enter species." name="species">
    <small id="speciesHelp" class="form-text text-muted">Enter this being's species</small>
  </div>
  <div class="form-group">
    <label for="gender">Gender</label>
    <input type="text" class="form-control" id="gender" aria-describedby="genderHelp" placeholder="Enter gender." name="gender">
    <small id="genderHelp" class="form-text text-muted">Enter this being's gender.</small>
  </div>
  <div class="form-group">
    <label for="height">Height</label>
    <input type="text" class="form-control" id="height" aria-describedby="heightHelp" placeholder="Enter height." name="height">
    <small id="heightHelp" class="form-text text-muted">Enter this being's height.</small>
  </div>
  <div class="form-group">
    <label for="mass">Mass</label>
    <input type="text" class="form-control" id="mass" aria-describedby="massHelp" placeholder="Enter Mass." name="mass">
    <small id="massHelp" class="form-text text-muted">Enter this being's mass/weight.</small>
  </div>
  <div class="form-group">
    <label for="hair">Hair</label>
    <input type="text" class="form-control" id="hair" aria-describedby="hairHelp" placeholder="Enter hair." name="hair">
    <small id="hairHelp" class="form-text text-muted">Enter a description of this being's hair.</small>
  </div>
  <div class="form-group">
    <label for="eyes">Eyes</label>
    <input type="text" class="form-control" id="eyes" aria-describedby="eyesHelp" placeholder="Enter eyes." name="eyes">
    <small id="eyesHelp" class="form-text text-muted">Enter a description of this being's eyes.</small>
  </div>
  <div class="form-group">
    <label for="skin">Skin</label>
    <input type="text" class="form-control" id="skin" aria-describedby="skinHelp" placeholder="Enter skin." name="skin">
    <small id="skinHelp" class="form-text text-muted">Enter a description of this being's skin.</small>
  </div>
  <div class="form-group">
    <label for="eras">Eras</label>
    <input type="text" class="form-control" id="eras" aria-describedby="erasHelp" placeholder="Enter eras." name="eras">
    <small id="erasHelp" class="form-text text-muted">Enter a comma separated list of eras this being was alive/active in.</small>
  </div>
  <div class="form-group">
    <label for="affiliation">Affiliation</label>
    <input type="text" class="form-control" id="affiliation" aria-describedby="affiliationHelp" placeholder="Enter affiliation." name="affiliation">
    <small id="affiliationHelp" class="form-text text-muted">Enter a comma separated list of organizations/groups this being was affiliated with.</small>
  </div>
</div>
</form>
</div>
</div>
@endsection
