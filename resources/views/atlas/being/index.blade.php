@extends('layouts.atlasmaster')
@section('title')
  Beings
@endsection
@section ('content')
      <div class="col-md-12">
        <h2>
          <span>Beings</span>
          <a href='/atlas/being/create'class='pull-right'><i class="fa fa-plus-square" aria-hidden="true"></i></a>
        </h2>
        <hr>
        @foreach ($beings as $being)
          @include('atlas.being.being')
        @endforeach
        {{-- <nav class="blog-pagination">
          <a class="btn btn-outline-primary" href="#">Older</a>
          <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
        </nav> --}}

      </div>
      {{ $beings->links() }}
@endsection
@section ('footer')
@endsection
