@extends('layouts.atlasmaster')
@section('title')
  {{ $being->name }}
@endsection
@section('content')
  @php
    $id = $being->id;
    $exists = Storage::disk('local')->exists('/public/atlasimages/being/'.$id.'.png');
  @endphp
<div class="col-md-12">
  <h1>
    {{ $being->name }}
  </h1>
  <small class="text-muted"><i class="fa fa-pencil-square" aria-hidden="true"></i> <a href="/atlas/being/{{ $being->id }}/edit">Edit this being</a></small>
  <hr>
  <div class="row">
    <div class="col-md-12">
<div class="infobox">
<table class="infoboxtable hidable-content" style="border-width: 0px" cellspacing="0" cellpadding="4">
<tbody>
  <tr>
    <td class="infoboximage" colspan="2" style="max-width: 250px; background: #736326;">
      @if ($exists == 1)
        <img alt="{{ $being->name }}" src="/storage/atlasimages/being/{{ $being->id }}.png" width="250px" data-toggle="modal" data-target="#imageModal">
      @else
        <a data-toggle="modal" data-target="#imageModal" ><i class="fa fa-upload" aria-hidden="true"></i> Click to upload image</a>
      @endif
    </td>
  </tr>
  <tr>
    <th class="infoboxheading" colspan="2" style="background: #736326;"> {{ $being->name }} </th>
  </tr>
  <tr>
    <th class="infoboxsubheading" colspan="2" style="background: #C2B170"><b>Biographical information</b></th>
  </tr>
  <tr>
    <td class="infoboxlabel" style="">Homeworld</td>
    <td class="infoboxcell" style=";"><p> {{ $being->homeworld }} </p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Born</td>
    <td class="infoboxcell" style=";"><p> {{ $being->born }} </p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Died</td>
    <td class="infoboxcell" style=";"><p> {{ $being->died }}</p></td>
  </tr>
  <tr>
    <th class="infoboxsubheading" colspan="2" style="background: #C2B170"><b>Physical Description</b></th>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Species</td>
    <td class="infoboxcell" style=";"><p> {{ $being->species }}</p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Gender</td>
    <td class="infoboxcell" style=";"><p> {{ $being->gender }}</p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Height</td>
    <td class="infoboxcell" style=";"><p> {{ $being->height }}</p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Mass</td>
    <td class="infoboxcell" style=";"><p> {{ $being->mass }}</p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Hair</td>
    <td class="infoboxcell" style=";"><p> {{ $being->hair }}</p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Eyes</td>
    <td class="infoboxcell" style=";"><p> {{ $being->eyes }}</p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Skin</td>
    <td class="infoboxcell" style=";"><p> {{ $being->skin }}</p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Eras</td>
    <td class="infoboxcell" style=";">
      <ul>
        @php
        $eras_data = $being->eras;
        $eras_list = str_getcsv($eras_data);
        foreach ( $eras_list as $eras ) {echo '<li>'.$eras.'</li>';}
        @endphp
      </ul></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Affiliation</td>
    <td class="infoboxcell" style=";">
      <ul>
        @php
        $affiliation_data = $being->affiliation;
        $affiliation_list = str_getcsv($affiliation_data);
        foreach ( $affiliation_list as $affiliation ) {echo '<li>'.$affiliation.'</li>';}
        @endphp
      </ul></td>
  </tr>
</tbody>
</table>
<span class="content-bg rbottom" style=""><span class="r4" style="background: #736326"></span><span class="r3" style="background: #736326"></span><span class="r2" style="background: #736326"></span><span class="r1" style="background: #736326"></span></span>
</div>
      {!! parsedown($being->description) !!}
      <hr>
      <small class="text-muted"><b><span class="oi oi-clock"></span> Last Updated at {{ $being->updated_at }}{{-- by {{ $being->updator->name }} --}}</b></small>
    </div>

  </div>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>
<!-- Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-width: 1310px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="imageModalLabel">Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img alt="{{ $being->name }}" src="/storage/atlasimages/being/{{ $being->id }}.png" width="1280px" align="middle">
        <hr>
        <form action="/atlas/being/{{ $being->id }}/image" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <label for="imageFile">Replace/Upload</label>
          <input type="file" name="imageFile">
          <small id="imageFileHelp" class="form-text text-muted">Upload a new file or replace the existing one.</small>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Upload</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
