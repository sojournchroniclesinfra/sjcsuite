@extends('layouts.albummaster')
@section('title')
  Entity Index
@endsection
@section ('content')

  <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Browse Entities</h1>
          <p class="lead text-muted">Select one of the entity types below to browse those entities.</p>
        </div>
      </section>
  <div class="album text-muted">
  <div class="container">
          <div class="row">
            <div class="card">
              <a href="/atlas/galaxy"><img style='width:100%;' src="/assets/images/entityCards/Galaxy.png" alt="Card image cap"></a>
              <p class="card-text">A system of millions or billions of stars, together with gas and dust, held together by gravitational attraction.</p>
            </div>
            <div class="card">
              <a href="/atlas/being"><img style='width:100%;' src="/assets/images/entityCards/Being.png" alt="Card image cap"></a>
              <p class="card-text">An individual, whether this is an entity beyond a person or just a single individual. A character.</p>
            </div>
            <div class="card">
              <a href="/atlas/world"><img style='width:100%;' src="/assets/images/entityCards/World.png" alt="Card image cap"></a>
              <p class="card-text">A celestial body that was in orbit around a star. Can be a moon or planet.</p>
            </div>

            <div class="card">
              <a href="/atlas/polity"><img style='width:100%;' src="/assets/images/entityCards/Polity.png" alt="Card image cap"></a>
              <p class="card-text">Any kind of political entity. It is a group of people who have a capacity to mobilize resources, and are organized by some form of institutionalized hierarchy.</p>
            </div>
            <div class="card">
              <a href="/atlas/city"><img style='width:100%;' src="/assets/images/entityCards/City.png" alt="Card image cap"></a>
              <p class="card-text">A large settlement of beings. Cities generally have extensive systems for housing, transportation, sanitation, utilities, land use, and communication.</p>
            </div>
            <div class="card">
              <a href="/atlas/species"><img style='width:100%;' src="/assets/images/entityCards/Species.png" alt="Card image cap"></a>
              <p class="card-text">The basic unit of biological classification and a taxonomic rank. Can be sentient or non-sentient.</p>
            </div>

            <div class="card">
              <a href="/atlas/system"><img style='width:100%;' src="/assets/images/entityCards/System.png" alt="Card image cap"></a>
              <p class="card-text">A set of gravitationally bound non-stellar objects in orbit around a star.</p>
            </div>
            <div class="card">
              <a href="/atlas/organization"><img style='width:100%;' src="/assets/images/entityCards/Organization.png" alt="Card image cap"></a>
              <p class="card-text">An entity comprising multiple people, such as an institution or an association that has a collective goal.</p>
            </div>
            <div class="card">
              <a href="/atlas/event"><img style='width:100%;' src="/assets/images/entityCards/Event.png" alt="Card image cap"></a>
              <p class="card-text">An occurrence; something that happens. A historical event.</p>
            </div>
            <div class="card">
              <a href="/atlas/law"><img style='width:100%;' src="/assets/images/entityCards/Law.png" alt="Card image cap"></a>
              <p class="card-text">A system of rules (or a single rule) that are created and enforced through social or governmental institutions to regulate behavior.</p>
            </div>
            <div class="card">
              <a href="/atlas/language"><img style='width:100%;' src="/assets/images/entityCards/Language.png" alt="Card image cap"></a>
              <p class="card-text">The use of complex systems of communication, and a language is any specific example of such a system.</p>
            </div>
            <div class="card">
              <a href="/atlas/object"><img style='width:100%;' src="/assets/images/entityCards/Object.png" alt="Card image cap"></a>
              <p class="card-text">A thing that has physical existence. An item. A piece of clothing or furniture.</p>
            </div>
            <div class="card">
              <a href="/atlas/weapon"><img style='width:100%;' src="/assets/images/entityCards/Weapon.png" alt="Card image cap"></a>
              <p class="card-text">any device used with intent to inflict damage or harm to living beings, structures, or systems.</p>
            </div>
            <div class="card">
              <a href="/atlas/vehicle"><img style='width:100%;' src="/assets/images/entityCards/Vehicle.png" alt="Card image cap"></a>
              <p class="card-text">a mobile machine that transports people or cargo.</p>
            </div>
            <div class="card">
              <a href="/atlas/currency"><img style='width:100%;' src="/assets/images/entityCards/Currency.png" alt="Card image cap"></a>
              <p class="card-text">Money in any form when in actual use or circulation as a medium of exchange.</p>
            </div>
          </div>
        </div>
        </div>
@endsection
@section ('footer')
@endsection
