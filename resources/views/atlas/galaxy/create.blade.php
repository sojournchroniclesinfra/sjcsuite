@extends('layouts.atlasmaster')
@section('content')
<div class="col-md-12">
  <h1>Create a Galaxy</h1>
  <hr>
<div class="row">
<div class="col-sm-8">
  <h3>Article Content</h3>
  <form method="POST" action="/atlas/galaxy">
    {{ csrf_field() }}
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter name." name="name">
    <small id="nameHelp" class="form-text text-muted">Enter the name of the galaxy.</small>
  </div>
  <div class="form-group">
    <label for="body">Description</label>
    <textarea class="form-control" name="description" id="description" aria-describedby="descriptionHelp" rows="8" cols="80"></textarea>
    <small id="descriptionHelp" class="form-text text-muted">Write a description of the galaxy.</small>
  </div>

<div class="form-group">
  <button type="submit" class="btn btn-primary">Publish</button>
</div>
<script>
var simplemde = new SimpleMDE({ element: document.getElementById("description") });
</script>
@include('layouts.errors')
</div>
<div class="col-sm-4">
  <h3>Entity Data</h3>
  <div class="form-group">
    <label for="starcount">Star Count</label>
    <input type="text" class="form-control" id="starcount" aria-describedby="starcountHelp" placeholder="Enter starcount." name="starcount">
    <small id="starcountHelp" class="form-text text-muted">Enter the number of stars in the galaxy. <b>Numbers only</b></small>
  </div>
  <div class="form-group">
    <label for="systemcount">System Count</label>
    <input type="text" class="form-control" id="systemcount" aria-describedby="systemcountHelp" placeholder="Enter systemcount." name="systemcount">
    <small id="systemcountHelp" class="form-text text-muted">Enter the number of systems with planetary bodies in the galaxy. <b>Numbers only</b></small>
  </div>
  <div class="form-group">
    <label for="size_ly">Diameter</label>
    <input type="text" class="form-control" id="size_ly" aria-describedby="size_lyHelp" placeholder="Enter Diameter in LY." name="size_ly">
    <small id="size_lyHelp" class="form-text text-muted">Enter the galaxy's diameter in light-years. <b>Numbers Only</b></small>
  </div>
  <div class="form-group">
    <label for="companion_galaxies">Companion Galaxies</label>
    <input type="text" class="form-control" id="companion_galaxies" aria-describedby="companion_galaxiesHelp" placeholder="Enter Companion Galaxies." name="companion_galaxies">
    <small id="companion_galaxiesHelp" class="form-text text-muted">Enter a comma-separated list of companion galaxies.</small>
  </div>

</div>
</form>
</div>
</div>
@endsection
