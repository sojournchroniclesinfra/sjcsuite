<div class="blog-post">
  <h2 class="blog-post-title">
    <a href="/atlas/galaxy/{{ $galaxy->id }}">
      {{ $galaxy->name }}
    </a>
  </h2>
  @if(Auth::user()->is_moderator == TRUE)
  <form id="galaxyForm{{ $galaxy->id }}" action="{{action('GalaxyController@destroy', $galaxy->id)}}" method="post">
    {{csrf_field()}}
    <input name="_method" type="hidden" value="DELETE">
    <button class="btn btn-outline-danger btn-sm delete-button" type="button">Delete</button>
    {{-- <a href="javascript:{}" class="entity_delete" onclick="form.submit();">Delete</a> --}}
  </form>
  @endif
  <p class="blog-post-meta">
    <ul>
      <small class="text-muted"><li>Creator: {{ $galaxy->user->name }}</li></small>
      <small class="text-muted"><li>Created: {{ $galaxy->created_at->toFormattedDateString() }}</li></small>
    </ul>
  </p>
  {{ str_limit($galaxy->description,150)}}
</div>
<script>
    $('.delete-button').on('click', function(e) {
        e.preventDefault();
        f=$(this).closest('form');
        swal({
            title: 'Are you sure?',
            text: "You're about to wipe out millions of innocent lives.",
            icon: 'warning',
            dangerMode: true,
            buttons: {
              cancel: {
                text: "Cancel",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
              },
              confirm: {
                text: "Delete",
                value: true,
                visible: true,
                className: "",
                closeModal: true
              }
            }
        })
        .then((willDelete) => {
          if (willDelete) {

            swal({
              title: "Galaxy Deleted",
              text: "Congratulations, you've deleted an entire galaxy.",
              icon: "success",
              buttons: false,
            });
            setTimeout(function() {
            f.submit();
          }, 1000);
          } else {
            swal({
              title: "Deletion Cancelled",
              text: "I'm sure everyone who lived there thanks you.",
              icon: "info",
            });
          }
        });
      });
</script>
