@extends('layouts.atlasmaster')
@section('title')
  Galaxies
@endsection
@section ('content')
      <div class="col-md-12">
        <h2>
          <span>Galaxies</span>
          <a href='/atlas/galaxy/create'class='pull-right'><i class="fa fa-plus-square" aria-hidden="true"></i></a>
        </h2>
        <hr>
        @foreach ($galaxies as $galaxy)
          @include('atlas.galaxy.galaxy')
        @endforeach
        {{-- <nav class="blog-pagination">
          <a class="btn btn-outline-primary" href="#">Older</a>
          <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
        </nav> --}}

      </div>
      {{ $galaxies->links() }}
@endsection
@section ('footer')
@endsection
