@extends('layouts.atlasmaster')
@section('title')
  {{ $galaxy->name }}
@endsection
@section('content')
  @php
    $id = $galaxy->id;
    $exists = Storage::disk('local')->exists('/public/atlasimages/galaxy/'.$id.'.png');
  @endphp
<div class="col-md-12">
  <h1>
    {{ $galaxy->name }}
  </h1>
  <small class="text-muted"><i class="fa fa-pencil-square" aria-hidden="true"></i> <a href="/atlas/galaxy/{{ $galaxy->id }}/edit">Edit this galaxy</a></small>
  <hr>
  <div class="row">
    <div class="col-md-12">
<div class="infobox">
<table class="infoboxtable hidable-content" style="border-width: 0px" cellspacing="0" cellpadding="4">
<tbody>
  <tr>
    <td class="infoboximage" colspan="2" style="max-width: 250px; background: #004466;">
      {{-- <img alt="{{ $galaxy->name }}" src="/assets/entityImages/Galaxy/1.png" data-file-width="1280" data-file-height="717" width="1280" height="717"> --}}
      @if ($exists == 1)
        <img alt="{{ $galaxy->name }}" src="/storage/atlasimages/galaxy/{{ $galaxy->id }}.png" width="250px" data-toggle="modal" data-target="#imageModal">
      @else
        <a data-toggle="modal" data-target="#imageModal" ><i class="fa fa-upload" aria-hidden="true"></i> Click to upload image</a>
      @endif
    </td>
  </tr>
  <tr>
    <th class="infoboxheading" colspan="2" style="background: #004466;"> {{ $galaxy->name }} </th>
  </tr>
  <tr>
    <th class="infoboxsubheading" colspan="2" style="background: #BBCCDD"><b>Astrographical information</b></th>
  </tr>
  <tr>
    <td class="infoboxlabel" style="">Total stars</td>
    <td class="infoboxcell" style=";"><p> {{ number_format($galaxy->starcount) }} </p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Total star systems</td>
    <td class="infoboxcell" style=";"><p> {{ number_format($galaxy->systemcount) }} </p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Diameter</td>
    <td class="infoboxcell" style=";"><p> {{ number_format($galaxy->size_ly) }} LY Longest axis </p></td>
  </tr>
  <tr>
    <td class="infoboxlabel" style=""> Companion Galaxies</td>
    <td class="infoboxcell" style=";">
      <ul>
        @php
        $companion_data = $galaxy->companion_galaxies;
        $companion_list = str_getcsv($companion_data);
        foreach ( $companion_list as $companion ) {echo '<li>'.$companion.'</li>';}
        @endphp
      </ul></td>
  </tr>
</tbody>
</table>
<span class="content-bg rbottom" style=""><span class="r4" style="background: #004466"></span><span class="r3" style="background: #004466"></span><span class="r2" style="background: #004466"></span><span class="r1" style="background: #004466"></span></span>
</div>
      {!! parsedown($galaxy->description) !!}
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-width: 1310px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="imageModalLabel">Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img alt="{{ $galaxy->name }}" src="/storage/atlasimages/galaxy/{{ $galaxy->id }}.png" width="1280px" align="middle">
        <hr>
        <form action="/atlas/galaxy/{{ $galaxy->id }}/image" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <label for="imageFile">Replace/Upload</label>
          <input type="file" name="imageFile">
          <small id="imageFileHelp" class="form-text text-muted">Upload a new file or replace the existing one.</small>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Upload</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
