@extends('layouts.atlasmaster')
@section('title')
  Home
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <div class="container-fluid">
            <h1>Atlas</h1>
            <p>Atlas is our repository of information about the setting of the Sojourn Chronicles.</p>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-4">
              <h2>Articles</h2>
              <p>Articles are pretty much exactly what the name implies. They're a single page of information, this can be about anything from the project itself to an in-universe topic.</p>
              <p><a class="btn btn-dark" href="/atlas/article" role="button">View Articles &raquo;</a></p>
            </div>
            <div class="col-md-4">
              <h2>Entities</h2>
              <p>An Entity is a single item in the setting. This can be anything from a galaxy to an article of clothing.</p>
              <p><a class="btn btn-dark" href="/atlas/browse" role="button" disabled>View entity index &raquo;</a></p>
            </div>
            <div class="col-md-4">
              <h2>Create a new Entity</h2>
              <p>If you'd like to create a new entity, click the link below to pick an entity type, and then get creating.</p>
              <p><a class="btn btn-dark" href="/atlas/create" role="button">Create Entity &raquo;</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
