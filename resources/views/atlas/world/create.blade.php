@extends('layouts.atlasmaster')
@section('title')
  Create a World
@endsection
@section('content')
<div class="col-md-12">
  <h1>Create a World</h1>
  <hr>
<div class="row">
<div class="col-sm-8">
  <h3>Article Content</h3>
  <form method="POST" action="/atlas/world">
    {{ csrf_field() }}
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter name." name="name">
    <small id="nameHelp" class="form-text text-muted">Enter the name of the world.</small>
  </div>
  <div class="form-group">
    <label for="body">Description</label>
    <textarea class="form-control" name="description" id="description" aria-describedby="descriptionHelp" rows="8" cols="80"></textarea>
    <small id="descriptionHelp" class="form-text text-muted">Write a description of the world.</small>
  </div>

<div class="form-group">
  <a href="/atlas/world" class="btn btn-light">Back</a>
  <button type="submit" class="btn btn-primary">Publish</button>
</div>
<script>
var simplemde = new SimpleMDE({ element: document.getElementById("description") });
</script>
@include('layouts.errors')
</div>
<div class="col-sm-4">
  <h3>Entity Data</h3>
  <div class="form-group">
    <label for="region">Region</label>
    <input type="text" class="form-control" id="region" aria-describedby="regionHelp" placeholder="Enter region." required name="region">
    <small id="regionHelp" class="form-text text-muted">Enter the region that this world resides within.</small>
  </div>
  <div class="form-group">
    <label for="sector">Sector</label>
    <input type="text" class="form-control" id="sector" aria-describedby="sectorHelp" placeholder="Enter sector." required name="sector">
    <small id="sectorHelp" class="form-text text-muted">Enter the sector that this world resides within.</small>
  </div>
  <div class="form-group">
    <label for="system">System</label>
    <input type="text" class="form-control" id="system" aria-describedby="systemHelp" placeholder="Enter system." required name="system">
    <small id="systemHelp" class="form-text text-muted">Enter the system that this world resides within.</small>
  </div>
  <div class="form-group">
    <label for="suns">Suns</label>
    <input type="text" class="form-control" id="suns" aria-describedby="sunsHelp" placeholder="Enter suns." required name="suns">
    <small id="sunsHelp" class="form-text text-muted">Enter a comma-separated list of suns that this world orbits.</small>
  </div>
  <div class="form-group">
    <label for="orbital_pos">Orbital Position</label>
    <input type="text" class="form-control" id="orbital_pos" aria-describedby="orbital_posHelp" placeholder="Enter orbital_pos." required name="orbital_pos">
    <small id="orbital_posHelp" class="form-text text-muted">Enter this world's orbital position, should be roman numerals. Earth's would be III.</small>
  </div>
  <div class="form-group">
    <label for="moons">Moons</label>
    <input type="text" class="form-control" id="moons" aria-describedby="moonsHelp" placeholder="Enter moons." required name="moons">
    <small id="moonsHelp" class="form-text text-muted">Enter a comma-separated list this worlds moons.</small>
  </div>
  <div class="form-group">
    <label for="rotation_period">Rotation Period</label>
    <input type="text" class="form-control" id="rotation_period" aria-describedby="rotation_periodHelp" placeholder="Enter rotation_period." required name="rotation_period">
    <small id="rotation_periodHelp" class="form-text text-muted">Enter this world's rotation period - this is how long it takes to rotate on its axis. It's day-length.</small>
  </div>
  <div class="form-group">
    <label for="orbital_period">Orbital Period</label>
    <input type="text" class="form-control" id="orbital_period" aria-describedby="orbital_periodHelp" placeholder="Enter orbital_period." required name="orbital_period">
    <small id="orbital_periodHelp" class="form-text text-muted">Enter this world's orbital period - this is how long it takes to rotate its parent body. It's year-length.</small>
  </div>
  <div class="form-group">
    <label for="class">Class</label>
    <input type="text" class="form-control" id="class" aria-describedby="classHelp" placeholder="Enter class." required name="class">
    <small id="classHelp" class="form-text text-muted">Enter this world's class. Examples are earth: terrestrial, jupiter: gas giant.</small>
  </div>
  <div class="form-group">
    <label for="diameter">Diameter</label>
    <input type="text" class="form-control" id="diameter" aria-describedby="diameterHelp" placeholder="Enter diameter." required name="diameter">
    <small id="diameterHelp" class="form-text text-muted">Enter this world's diameter</small>
  </div>
  <div class="form-group">
    <label for="atmosphere">Atmosphere</label>
    <input type="text" class="form-control" id="atmosphere" aria-describedby="atmosphereHelp" placeholder="Enter atmosphere." required name="atmosphere">
    <small id="atmosphereHelp" class="form-text text-muted">Enter this world's atmosphere type. Most common: Type-I breathable.</small>
  </div>
  <div class="form-group">
    <label for="gravity">Gravity</label>
    <input type="text" class="form-control" id="gravity" aria-describedby="gravityHelp" placeholder="Enter gravity." required name="gravity">
    <small id="gravityHelp" class="form-text text-muted">Enter this world's gravity in G's. Don't enter the G. <b>Numbers only.</b></small>
  </div>
  <div class="form-group">
    <label for="terrain">Terrain</label>
    <input type="text" class="form-control" id="terrain" aria-describedby="terrainHelp" placeholder="Enter terrain." required name="terrain">
    <small id="terrainHelp" class="form-text text-muted">Enter this world's most common terrain.</small>
  </div>
  <div class="form-group">
    <label for="surface_water">Surface water</label>
    <input type="text" class="form-control" id="surface_water" aria-describedby="surface_waterHelp" placeholder="Enter surface_water." required name="surface_water">
    <small id="surface_waterHelp" class="form-text text-muted">Enter this world's surface water percentage. Do not include the percent symbol. <b>Numbers only.</b></small>
  </div>
  <div class="form-group">
    <label for="poi">Points of interest</label>
    <input type="text" class="form-control" id="poi" aria-describedby="poiHelp" placeholder="Enter poi." required name="poi">
    <small id="poiHelp" class="form-text text-muted">Enter a comma-seperated list of points of interest on this world.</small>
  </div>
  <div class="form-group">
    <label for="flora">Native Flora</label>
    <input type="text" class="form-control" id="flora" aria-describedby="floraHelp" placeholder="Enter flora." required name="flora">
    <small id="floraHelp" class="form-text text-muted">Enter a comma-seperated list of common plants on this world.</small>
  </div>
  <div class="form-group">
    <label for="fauna">Native Fauna</label>
    <input type="text" class="form-control" id="fauna" aria-describedby="faunaHelp" placeholder="Enter fauna." required name="fauna">
    <small id="faunaHelp" class="form-text text-muted">Enter a comma-seperated list of common creatures on this world.</small>
  </div>
  <div class="form-group">
    <label for="native_species">Native Sentient Species</label>
    <input type="text" class="form-control" id="native_species" aria-describedby="native_speciesHelp" placeholder="Enter native_species." required name="native_species">
    <small id="native_speciesHelp" class="form-text text-muted">Enter a comma-seperated list of native sentient species on this world.</small>
  </div>
  <div class="form-group">
    <label for="immigrated_species">Immigrated Sentient Species</label>
    <input type="text" class="form-control" id="immigrated_species" aria-describedby="immigrated_speciesHelp" placeholder="Enter immigrated_species." required name="immigrated_species">
    <small id="immigrated_speciesHelp" class="form-text text-muted">Enter a comma-seperated list of immigrated sentient species on this world.</small>
  </div>
  <div class="form-group">
    <label for="languages">Official Languages</label>
    <input type="text" class="form-control" id="languages" aria-describedby="languagesHelp" placeholder="Enter languages." required name="languages">
    <small id="languagesHelp" class="form-text text-muted">Enter a comma-seperated list of official languages on this world.</small>
  </div>
  <div class="form-group">
    <label for="government">Government</label>
    <input type="text" class="form-control" id="government" aria-describedby="governmentHelp" placeholder="Enter government." required name="government">
    <small id="governmentHelp" class="form-text text-muted">Enter the type of government this world is subject to.</small>
  </div>
  <div class="form-group">
    <label for="population">Population</label>
    <input type="text" class="form-control" id="population" aria-describedby="populationHelp" placeholder="Enter population." required name="population">
    <small id="populationHelp" class="form-text text-muted">Enter this world's population. <b>Numbers only.</b></small>
  </div>
  <div class="form-group">
    <label for="cities">Notable Cities</label>
    <input type="text" class="form-control" id="cities" aria-describedby="citiesHelp" placeholder="Enter cities." required name="cities">
    <small id="citiesHelp" class="form-text text-muted">Enter a comma-seperated list of major cities on this world.</small>
  </div>
  <div class="form-group">
    <label for="imports">Major Imports</label>
    <input type="text" class="form-control" id="imports" aria-describedby="importsHelp" placeholder="Enter imports." required name="imports">
    <small id="importsHelp" class="form-text text-muted">Enter a comma-seperated list of major imports on this world.</small>
  </div>
  <div class="form-group">
    <label for="exports">Major Exports</label>
    <input type="text" class="form-control" id="exports" aria-describedby="exportsHelp" placeholder="Enter exports." required name="exports">
    <small id="exportsHelp" class="form-text text-muted">Enter a comma-seperated list of major exports on this world.</small>
  </div>
  <div class="form-group">
    <label for="affiliation">Affiliation</label>
    <input type="text" class="form-control" id="affiliation" aria-describedby="affiliationHelp" placeholder="Enter affiliation." required name="affiliation">
    <small id="affiliationHelp" class="form-text text-muted">Enter a comma-seperated list of organizations this world is or has been affiliated with.</small>
  </div>
</div>
</form>
</div>
</div>
@endsection
