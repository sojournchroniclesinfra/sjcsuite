@extends('layouts.atlasmaster')
@section('title')
  Worlds
@endsection
@section ('content')
      <div class="col-md-12">
        <h2>
          <span>Worlds</span>
          <a href='/atlas/world/create'class='pull-right'><i class="fa fa-plus-square" aria-hidden="true"></i></a>
        </h2>
        <hr>
        @foreach ($worlds as $world)
          @include('atlas.world.world')
        @endforeach
        {{-- <nav class="blog-pagination">
          <a class="btn btn-outline-primary" href="#">Older</a>
          <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
        </nav> --}}

      </div>
      {{ $worlds->links() }}
@endsection
@section ('footer')
@endsection
