@extends('layouts.atlasmaster')
@section('title')
  {{ $world->name }}
@endsection
@section('content')
  @php
    $id = $world->id;
    $exists = Storage::disk('local')->exists('/public/atlasimages/world/'.$id.'.png');
  @endphp
<div class="col-md-12">
  <h1>
    {{ $world->name }}
  </h1>
  <small class="text-muted"><i class="fa fa-pencil-square" aria-hidden="true"></i> <a href="/atlas/world/{{ $world->id }}/edit">Edit this world</a></small>

  <hr>
  <div class="row">
    <div class="col-md-12">
<div class="infobox">
<table class="infoboxtable hidable-content" style="border-width: 0px" cellspacing="0" cellpadding="4">
<tbody>
  <tr>
    <td class="infoboximage" colspan="2" style="max-width: 250px; background: #999966;">
      {{-- <small><a ><i class="fa fa-upload" aria-hidden="true"></i> Upload Image</a> --}}
      @if ($exists == 1)
        <img alt="{{ $world->name }}" src="/storage/atlasimages/world/{{ $world->id }}.png" width="250px" data-toggle="modal" data-target="#imageModal">
      @else
        <a data-toggle="modal" data-target="#imageModal" ><i class="fa fa-upload" aria-hidden="true"></i> Click to upload image</a>
      @endif

    </td>
  </tr>
  <tr>
    <th class="infoboxheading" colspan="2" style="background: #999966;"> {{ $world->name }} </th>
  </tr>
  <tr>
    <th class="infoboxsubheading" colspan="2" style="background: #DDDDBB"><b>Astrographical information</b></th>
  </tr>
  @if (!empty($world->region))
  <tr>
    <td class="infoboxlabel" style="">Region</td>
    <td class="infoboxcell" style=";"><p> {{ $world->region }} </p></td>
  </tr>
  @endif
  @if (!empty($world->sector))
  <tr>
    <td class="infoboxlabel" style=""> Sector</td>
    <td class="infoboxcell" style=";"><p> {{ $world->sector }}</p></td>
  </tr>
  @endif
  @if (!empty($world->system))
  <tr>
    <td class="infoboxlabel" style=""> System</td>
    <td class="infoboxcell" style=";"><p> {{ $world->system }}</p></td>
    </td>
  </tr>
  @endif
  @if (!empty($world->suns))
  <tr>
    <td class="infoboxlabel" style=""> Suns</td>
    <td class="infoboxcell" style=";">
        @php
        $suns_data = $world->suns;
        $suns_list = str_getcsv($suns_data);
        $sun_count = count($suns_list);
        if($sun_count == 1){
          echo $suns_data;
        } else {
          echo '<ul>';
          echo '<li>'.$sun_count.'</li>';
          echo '<ul>';
          foreach ( $suns_list as $suns ) {echo '<li>'.$suns.'</li>';}
          echo '</ul>';
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->orbital_pos))
  <tr>
    <td class="infoboxlabel" style=""> Orbital Position</td>
    <td class="infoboxcell" style=";"><p> {{ $world->orbital_pos }}</p></td>
  </tr>
  @endif
  @if (!empty($world->moons))
  <tr>
    <td class="infoboxlabel" style=""> Moons</td>
    <td class="infoboxcell" style=";">
        @php
        $moonsdata = $world->moons;
        $moonslist = str_getcsv($moonsdata);
        $moon_count = count($moonslist);
        if($moon_count == 1) {
          echo $moonsdata;
        } else {
          echo '<ul>';
          echo '<li>'.$moon_count.'</li>';
          echo '<ul>';
          foreach ( $moonslist as $moons ) {echo '<li>'.$moons.'</li>';}
          echo '</ul>';
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->rotation_period))
  <tr>
    <td class="infoboxlabel" style=""> Rotational Period</td>
    <td class="infoboxcell" style=";"><p> {{ $world->rotation_period }}</p></td>
  </tr>
  @endif
  @if (!empty($world->orbital_period))
  <tr>
    <td class="infoboxlabel" style=""> Orbital Period</td>
    <td class="infoboxcell" style=";"><p> {{ $world->orbital_period }}</p></td>
  </tr>
  @endif
  <tr>
    <th class="infoboxsubheading" colspan="2" style="background: #DDDDBB"><b>Physical information</b></th>
  </tr>
  @if (!empty($world->class))
    <tr>
      <td class="infoboxlabel" style=""> Class</td>
      <td class="infoboxcell" style=";"><p> {{ $world->class }}</p></td>
    </tr>
  @endif
  @if (!empty($world->diameter))
  <tr>
    <td class="infoboxlabel" style=""> Diameter</td>
    <td class="infoboxcell" style=";"><p> {{ number_format($world->diameter) }}</p></td>
  </tr>
  @endif
  @if (!empty($world->atmosphere))
  <tr>
    <td class="infoboxlabel" style=""> Atmosphere</td>
    <td class="infoboxcell" style=";"><p> {{ $world->atmosphere }}</p></td>
  </tr>
  @endif
  @if (!empty($world->gravity))
  <tr>
    <td class="infoboxlabel" style=""> Gravity</td>
    <td class="infoboxcell" style=";"><p> {{ $world->gravity }}</p></td>
  </tr>
  @endif
  @if (!empty($world->terrain))
  <tr>
    <td class="infoboxlabel" style=""> Terrain</td>
    <td class="infoboxcell" style=";"><p> {{ $world->terrain }}</p></td>
  </tr>
  @endif
  @if (!empty($world->surface_water))
  <tr>
    <td class="infoboxlabel" style=""> Surface Water</td>
    <td class="infoboxcell" style=";"><p> {{ $world->surface_water }}</p></td>
  </tr>
  @endif
  <tr>
    <th class="infoboxsubheading" colspan="2" style="background: #DDDDBB"><b>Societal information</b></th>
  </tr>
  @if (!empty($world->poi))
  <tr>
    <td class="infoboxlabel" style=""> Points of Interest</td>
    <td class="infoboxcell" style=";">
        @php
        $poidata = $world->poi;
        $poilist = str_getcsv($poidata);
        $poi_count = count($poilist);
        if($poi_count == 1){
          echo $poidata;
        } else {
          echo '<ul>';
          foreach ( $poilist as $poi ) {echo '<li>'.$poi.'</li>';}
          echo '</ul>';
        }
        @endphp

    </td>
  </tr>
  @endif
  @if (!empty($world->flora))
  <tr>
    <td class="infoboxlabel" style=""> Flora</td>
    <td class="infoboxcell" style=";">
        @php
        $flora_data = $world->flora;
        $flora_list = str_getcsv($flora_data);
        $flora_count = count($flora_list);
        if($flora_count == 1) {
          echo $flora_data;
        } else {
          echo '<ul>';
          foreach ( $flora_list as $flora ) {echo '<li>'.$flora.'</li>';}
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->fauna))
  <tr>
    <td class="infoboxlabel" style=""> Fauna</td>
    <td class="infoboxcell" style=";">
        @php
        $fauna_data = $world->fauna;
        $fauna_list = str_getcsv($fauna_data);
        $fauna_count = count($fauna_list);
        if($fauna_count == 1) {
          echo $fauna_data;
        } else {
          echo '<ul>';
          foreach ( $fauna_list as $fauna ) {echo '<li>'.$fauna.'</li>';}
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->native_species))
  <tr>
    <td class="infoboxlabel" style=""> Native Species</td>
    <td class="infoboxcell" style=";">
        @php
        $native_species_data = $world->native_species;
        $native_species_list = str_getcsv($native_species_data);
        $native_species_count = count($native_species_list);
        if($native_species_count == 1) {
          echo $native_species_data;
        } else {
          echo '<ul>';
          foreach ( $native_species_list as $native_species ) {echo '<li>'.$native_species.'</li>';}
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->immigrated_species))
  <tr>
    <td class="infoboxlabel" style=""> Immigrated Species</td>
    <td class="infoboxcell" style=";">

        @php
        $immigrated_species_data = $world->immigrated_species;
        $immigrated_species_list = str_getcsv($immigrated_species_data);
        $immigrated_species_count = count($immigrated_species_list);
        if($immigrated_species_count == 1) {
          echo $immigrated_species_data;
        } else {
          echo '<ul>';
          foreach ( $immigrated_species_list as $immigrated_species ) {echo '<li>'.$immigrated_species.'</li>';}
          echo '</ul>';
        }
        @endphp

    </td>
  </tr>
  @endif
  @if (!empty($world->languages))
  <tr>
    <td class="infoboxlabel" style=""> Languages</td>
    <td class="infoboxcell" style=";">
        @php
        $languages_data = $world->languages;
        $languages_list = str_getcsv($languages_data);
        $languages_count = count($languages_data);
        if($languages_count == 1) {
          echo $languages_data;
        } else {
          echo '<ul>';
          foreach ( $languages_list as $languages ) {echo '<li>'.$languages.'</li>';}
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->government))
  <tr>
    <td class="infoboxlabel" style=""> Government</td>
    <td class="infoboxcell" style=";"><p> {{ $world->government }}</p></td>
  </tr>
  @endif
  @if (!empty($world->government))
  <tr>
    <td class="infoboxlabel" style=""> Population</td>
    <td class="infoboxcell" style=";"><p> {{ $world->population }}</p></td>
  </tr>
  @endif
  @if (!empty($world->cities))
  <tr>
    <td class="infoboxlabel" style=""> Cities</td>
    <td class="infoboxcell" style=";">
        @php
        $cities_data = $world->cities;
        $cities_list = str_getcsv($cities_data);
        $cities_count = count($cities_list);
        if($cities_count == 1) {
          echo $cities_data;
        } else {
          echo '<ul>';
          foreach ( $cities_list as $cities ) {echo '<li>'.$cities.'</li>';}
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->imports))
  <tr>
    <td class="infoboxlabel" style=""> Major Imports</td>
    <td class="infoboxcell" style=";">
        @php
        $imports_data = $world->imports;
        $imports_list = str_getcsv($imports_data);
        $imports_count = count($imports_list);
        if($imports_count == 1) {
          echo $imports_data;
        } else {
          echo '<ul>';
          foreach ( $imports_list as $imports ) {echo '<li>'.$imports.'</li>';}
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->exports))
  <tr>
    <td class="infoboxlabel" style=""> Major Exports</td>
    <td class="infoboxcell" style=";">
        @php
        $exports_data = $world->exports;
        $exports_list = str_getcsv($exports_data);
        $exports_count = count($exports_list);
        if($exports_count == 1) {
          echo $exports_data;
        } else {
          echo '<ul>';
          foreach ( $exports_list as $exports ) {echo '<li>'.$exports.'</li>';}
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
  @if (!empty($world->affiliation))
  <tr>
    <td class="infoboxlabel" style=""> Affiliation</td>
    <td class="infoboxcell" style=";">
        @php
        $affiliation_data = $world->affiliation;
        $affiliation_list = str_getcsv($affiliation_data);
        $affiliation_count = count($affiliation_list);
        if($affiliation_count == 1) {
          echo $affiliation_data;
        } else {
          echo '<ul>';
          foreach ( $affiliation_list as $affiliation ) {echo '<li>'.$affiliation.'</li>';}
          echo '</ul>';
        }
        @endphp
    </td>
  </tr>
  @endif
</tbody>
</table>
<span class="content-bg rbottom" style=""><span class="r4" style="background: #999966"></span><span class="r3" style="background: #999966"></span><span class="r2" style="background: #999966"></span><span class="r1" style="background: #999966"></span></span>
</div>
      {!! parsedown($world->description) !!}
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-width: 1310px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="imageModalLabel">Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img alt="{{ $world->name }}" src="/storage/atlasimages/world/{{ $world->id }}.png" width="1280px" align="middle">
        <hr>
        <form action="/atlas/world/{{ $world->id }}/image" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <label for="imageFile">Replace/Upload</label>
          <input type="file" name="imageFile">
          <small id="imageFileHelp" class="form-text text-muted">Upload a new file or replace the existing one.</small>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Upload</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
