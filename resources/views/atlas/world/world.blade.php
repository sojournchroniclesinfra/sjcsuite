<div class="blog-post">
  <h2 class="blog-post-title">
    <a href="/atlas/world/{{ $world->id }}">
      {{ $world->name }}
    </a>
  </h2>
  @if(Auth::user()->is_moderator == TRUE)
  <form action="{{action('WorldController@destroy', $world->id)}}" method="post">
    {{csrf_field()}}
    <input name="_method" type="hidden" value="DELETE">
    <button class="btn btn-outline-danger btn-sm" type="submit">Delete</button>
    {{-- <a href="javascript:{}" class="entity_delete" onclick="form.submit();">Delete</a> --}}
  </form>
  @endif
  <p class="blog-post-meta">
    <ul>
      <small class="text-muted"><li>Creator: {{ $world->user->name }}</li></small>
      <small class="text-muted"><li>Created: {{ $world->created_at->toFormattedDateString() }}</li></small>
    </ul>
  </p>
  {{ str_limit($world->description,150)}}
</div>
