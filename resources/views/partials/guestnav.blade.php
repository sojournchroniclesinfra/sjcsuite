<nav class="navbar fixed-top navbar-expand-md navbar-dark navbar-custom">
    <a class="navbar-brand" href="/">
              <img src="/assets/images/nav-brand.svg" width="30" height="30" alt=""> Sojourn Chronicles
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
        @if (Auth::check())
        <ul class="nav navbar-nav">
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}<span class="caret"></span></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/auth/settings"><span class="oi oi-cog"></span> User Profile/Settings</a>
                    <a class="dropdown-item" href="/auth/update-selfpass"><span class="oi oi-key"></span> Change Password</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="oi oi-account-logout"></span> Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="oi oi-menu"></span></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <h6 class="dropdown-header">Site Navigation</h6>
                    <a class="dropdown-item" href="/">Main Portal</a>
                    <a class="dropdown-item" href="/tickets/">Tickets</a>
                    <a class="dropdown-item" href="/cartographer/">Cartographer</a>
                    <a class="dropdown-item" href="/admin">Administration</a>
                    <div class="dropdown-divider"></div>
                    <h6 class="dropdown-header">Help</h6>
                        <a class="dropdown-item" href="/kb">Documentation</a>
                        <a class="dropdown-item" href="/helpdesk">Helpdesk</a>
                </div>
            </li>
        </ul>
        @endif
        @guest
          <ul class="nav navbar-nav">
            <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
          </ul>
        @endguest
    </div>
</nav>
