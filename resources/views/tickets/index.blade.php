@extends('layouts.tools')
@section('title')
  My Tickets
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <h2>My Tickets</h2>
        <table class="table">
          <thead>
            <tr>
              <td>ID</td>
              <td>Project</td>
              <td>Status</td>
              <td>Summary</td>
              <td>Assignee</td>
              <td>Creator</td>
              <td>Created On</td>
            </tr>
          </thead>
          <tbody class="table-striped">
            @foreach ($tickets as $ticket)
              @include('tickets.ticket')
              <hr>
            @endforeach
          </tbody>
        </table>
      {{ $tickets->links() }}
      </div>
    </div>
  </div>
@endsection
