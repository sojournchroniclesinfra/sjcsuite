<tr>
  <td>
    <a href="/tickets/details/{{ $ticket->id }}">
      {{ $ticket->id }}
    </a>
  </td>
  <td>
    {{ $ticket->project }}
  </td>
  <td>
    {{ $ticket->status }}
  </td>
  <td>
    {{ $ticket->summary }}
  </td>
  <td>
    {{ $ticket->assignee->name }}
  </td>
  <td>
    {{ $ticket->creator->name }}
  </td>
  <td>
    {{ $ticket->created_at->toFormattedDateString() }}
  </td>
</tr>
