@extends('layouts.tools')
@section('title')
  Home
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <div class="container-fluid">
            <h1>Sojourn Chronicles</h1>
            <h2>Internal Tools</h2>
            <p>Use the links below, or the dropdowns in the navigation bar to navigate these tools.</p>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-4">
              <h2>Tickets</h2>
              <p>Use these to track tasks that need to be done. </p>
              <p><a class="btn btn-outline-primary" href="/tickets" role="button">Tickets &raquo;</a></p>
            </div>
            <div class="col-md-4">
              <h2>Current Projects</h2>
              <p>Projects currently falling under the Sojourn Chronicles.</p>
              <p><a class="btn btn-outline-primary" href="/projects" role="button" disabled>Projects &raquo;</a></p>
            </div>
            <div class="col-md-4">
              <h2>Cartographer</h2>
              <p>Cartographer allows editing of articles in Atlas, our wiki-like solution.</p>
              <p><a class="btn btn-outline-primary" href="/cartographer" role="button">Cartographer &raquo;</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
