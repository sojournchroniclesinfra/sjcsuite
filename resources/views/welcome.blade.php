@extends('layouts.master')
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <div class="container-fluid">
            <h1>Sojourn Chronicles</h1>
            <p>A Science Fiction <a target="_blank" href="https://www.reddit.com/r/worldbuilding/wiki/index#wiki_frequently_asked_questions">worldbuilding</a> project.</p>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-4">
              <h2>About the Project</h2>
              <p>Sojourn Chronicles is a worldbuilding project set in a galaxy (far far away, don't sue me this isn't the real tagline)</p>
              <p>The project takes place in a presently-unnamed galaxy, and primarily revolves around the Prax'yi Domain, 1700 worlds linked together by a series of portals. Each world with a different tech level, climate, and/or sentient race.</p>
              <p>The Prax'yi are a seemingly benevolent advanced race who are running terraforming experiments.</p>
              <p><a class="btn btn-dark" href="/about" role="button">Read More &raquo;</a></p>
            </div>
            <div class="col-md-4">
              <h2>How Do I Contribute?</h2>
              <p>Currently we're looking for the following types of contributors:</p>
              <ul>
                <li><b>Creative Team: </b>We need people to write content within the setting that has been built already, whether this is short stories or novels.</li>
                <li><b>Architects: </b>We need people to continue building the setting, while this certainly invovles the creation of story for things like history, Architects focus on creating the setting for the Creatives.</li>
              </ul>
              <p>If you're interested in contributing, please click the button below to fill out an application.</p>
              <p><a class="btn btn-dark" href="/contribute" role="button" disabled>Contribute &raquo;</a></p>
            </div>
            <div class="col-md-4">
              <h2>Let's see some content</h2>
              <p>We're using an in-house system for displaying the content we've built. This system is called "Atlas". Atlas is currently only visible to members, but we're planning on changing this shortly.</p>
              <p>Check back for updates soon!</p>
              <p><a class="btn btn-dark" href="/atlas" role="button">Atlas &raquo;</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
