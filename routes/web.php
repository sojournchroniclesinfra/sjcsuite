<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
  Public Site
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contribute', 'MembershipInquiryController@index')->name('contribute');
Route::get('/contribute/create', 'MembershipInquiryController@create');
Route::get('/contribute/{membershipinquiry}', 'MembershipInquiryController@show');
/*
  Internal Site for users of the project.
*/
Route::get('/tools', 'ToolsController@index');
Route::get('/user/change-password', 'SelfServicePasswordController@index');
Route::patch('/user/change-password/{user}', 'SelfServicePasswordController@update');
Route::get('/atlas', 'AtlasController@index');
/*
  Ticketing System
*/
Route::get('/tickets', 'TicketController@index');
Route::get('/tickets/queues', 'TicketController@queueIndex');
Route::get('/tickets/details/{ticket}', 'TicketController@show');
Route::get('/tickets/create', 'TicketController@create');
Route::post('/tickets', 'TicketController@store');
/*
  Atlas Article Routes
*/
Route::get('/atlas/article/create', 'AtlasController@create'); ## Atlas article creation form.
Route::post('/atlas/article', 'AtlasController@store'); ## Atlas article post endpoint.
Route::get('/atlas/article', 'AtlasController@articleindex'); ## List all atlas articles.
Route::get('/atlas/article', 'AtlasController@articleindex'); ## List all atlas articles.
Route::get('/atlas/article/{article}', 'AtlasController@show'); ## Show a specific article.
Route::delete('/atlas/article/{article}', 'AtlasController@destroy'); ## Delete a specific article.
Route::get('/atlas/article/{article}/edit', 'AtlasController@edit'); ## Delete a specific article.
Route::patch('/atlas/article/{article}', 'AtlasController@update'); ## Delete a specific article.

## Atlas Entity Creation Landing Page
Route::get('/atlas/create', 'AtlasController@entitycreate');
Route::get('/atlas/browse', 'AtlasController@entitybrowse');
## Atlas Entity Indicies
Route::get('/atlas/galaxy', 'GalaxyController@index');
Route::get('/atlas/system', 'StarSystemController@index');
Route::get('/atlas/world', 'WorldController@index');
Route::get('/atlas/polity', 'PolityController@index');
Route::get('/atlas/city', 'CityController@index');
Route::get('/atlas/being', 'BeingController@index');
Route::get('/atlas/weapon', 'WeaponController@index');
Route::get('/atlas/object', 'ObjectController@index');
Route::get('/atlas/event', 'EventController@index');
Route::get('/atlas/species', 'EventController@index');
Route::get('/atlas/language', 'EventController@index');
Route::get('/atlas/law', 'EventController@index');
Route::get('/atlas/organization', 'OrganizationController@index');
Route::get('/atlas/currency', 'CurrencyController@index');
## Atlas Entity Creation
Route::get('/atlas/galaxy/create', 'GalaxyController@create');
Route::get('/atlas/system/create', 'StarSystemController@create');
Route::get('/atlas/world/create', 'WorldController@create');
Route::get('/atlas/polity/create', 'PolityController@create');
Route::get('/atlas/city/create', 'CityController@create');
Route::get('/atlas/being/create', 'BeingController@create');
Route::get('/atlas/weapon/create', 'WeaponController@create');
Route::get('/atlas/object/create', 'ObjectController@create');
Route::get('/atlas/event/create', 'EventController@create');
Route::get('/atlas/species/create', 'EventController@create');
Route::get('/atlas/language/create', 'EventController@create');
Route::get('/atlas/law/create', 'EventController@create');
Route::get('/atlas/organization/create', 'OrganizationController@create');
Route::get('/atlas/currency/create', 'CurrencyController@create');
## Atlas Entity Edit
Route::get('/atlas/galaxy/{galaxy}/edit', 'GalaxyController@edit');
Route::get('/atlas/system/{system}/edit', 'StarSystemController@edit');
Route::get('/atlas/world/{world}/edit', 'WorldController@edit');
Route::get('/atlas/polity/{polity}/edit', 'PolityController@edit');
Route::get('/atlas/city/{city}/edit', 'CityController@edit');
Route::get('/atlas/being/{being}/edit', 'BeingController@edit');
Route::get('/atlas/weapon/{weapon}/edit', 'WeaponController@edit');
Route::get('/atlas/object/{object}/edit', 'ObjectController@edit');
Route::get('/atlas/event/{event}/edit', 'EventController@edit');
Route::get('/atlas/species/{species}/edit', 'EventController@edit');
Route::get('/atlas/language/{language}/edit', 'EventController@edit');
Route::get('/atlas/law/{law}/edit', 'EventController@edit');
Route::get('/atlas/organization/{organization}/edit', 'OrganizationController@edit');
Route::get('/atlas/currency/{currency}/edit', 'CurrencyController@edit');
## Atlas Entity Details
Route::get('/atlas/galaxy/{galaxy}', 'GalaxyController@show');
Route::get('/atlas/system/{system}', 'StarSystemController@show');
Route::get('/atlas/world/{world}', 'WorldController@show');
Route::get('/atlas/polity/{polity}', 'PolityController@show');
Route::get('/atlas/city/{city}', 'CityController@show');
Route::get('/atlas/being/{being}', 'BeingController@show');
Route::get('/atlas/weapon/{weapon}', 'WeaponController@show');
Route::get('/atlas/object/{object}', 'ObjectController@show');
Route::get('/atlas/event/{event}', 'EventController@show');
Route::get('/atlas/species/{species}', 'EventController@show');
Route::get('/atlas/language/{language}', 'EventController@show');
Route::get('/atlas/law/{law}', 'EventController@show');
Route::get('/atlas/organization/{organization}', 'OrganizationController@show');
Route::get('/atlas/currency/{currency}', 'CurrencyController@show');
## update
Route::patch('/atlas/galaxy/{galaxy}', 'GalaxyController@update');
Route::patch('/atlas/system/{system}', 'StarSystemController@update');
Route::patch('/atlas/world/{world}', 'WorldController@update');
Route::patch('/atlas/polity/{polity}', 'PolityController@update');
Route::patch('/atlas/city/{city}', 'CityController@update');
Route::patch('/atlas/being/{being}', 'BeingController@update');
Route::patch('/atlas/weapon/{weapon}', 'WeaponController@update');
Route::patch('/atlas/object/{object}', 'ObjectController@update');
Route::patch('/atlas/event/{event}', 'EventController@update');
Route::patch('/atlas/species/{species}', 'EventController@update');
Route::patch('/atlas/language/{language}', 'EventController@update');
Route::patch('/atlas/law/{law}', 'EventController@update');
Route::patch('/atlas/organization/{organization}', 'OrganizationController@update');
Route::patch('/atlas/currency/{currency}', 'CurrencyController@update');
## update
Route::delete('/atlas/galaxy/{galaxy}', 'GalaxyController@destroy');
Route::delete('/atlas/system/{system}', 'StarSystemController@destroy');
Route::delete('/atlas/world/{world}', 'WorldController@destroy');
Route::delete('/atlas/polity/{polity}', 'PolityController@destroy');
Route::delete('/atlas/city/{city}', 'CityController@destroy');
Route::delete('/atlas/being/{being}', 'BeingController@destroy');
Route::delete('/atlas/weapon/{weapon}', 'WeaponController@destroy');
Route::delete('/atlas/object/{object}', 'ObjectController@destroy');
Route::delete('/atlas/event/{event}', 'EventController@destroy');
Route::delete('/atlas/species/{species}', 'EventController@destroy');
Route::delete('/atlas/language/{language}', 'EventController@destroy');
Route::delete('/atlas/law/{law}', 'EventController@destroy');
Route::delete('/atlas/organization/{organization}', 'OrganizationController@destroy');
Route::delete('/atlas/currency/{currency}', 'CurrencyController@destroy');
## Atlas Entity Stores
Route::post('/atlas/galaxy', 'GalaxyController@store');
Route::post('/atlas/system', 'StarSystemController@store');
Route::post('/atlas/world', 'WorldController@store');
Route::post('/atlas/polity', 'PolityController@store');
Route::post('/atlas/city', 'CityController@store');
Route::post('/atlas/being', 'BeingController@store');
Route::post('/atlas/weapon', 'WeaponController@store');
Route::post('/atlas/object', 'ObjectController@store');
Route::post('/atlas/event', 'EventController@store');
Route::post('/atlas/species', 'EventController@store');
Route::post('/atlas/language', 'EventController@store');
Route::post('/atlas/law', 'EventController@store');
Route::post('/atlas/organization', 'OrganizationController@store');
Route::post('/atlas/currency', 'CurrencyController@store');

## Atlas Entity Image Upload
Route::post('/atlas/galaxy/{galaxy}/image', 'GalaxyController@imageUpload');
Route::post('/atlas/system/{system}/image', 'StarSystemController@imageUpload');
Route::post('/atlas/world/{world}/image', 'WorldController@imageUpload');
Route::post('/atlas/polity/{polity}/image', 'PolityController@imageUpload');
Route::post('/atlas/city/{city}/image', 'CityController@imageUpload');
Route::post('/atlas/being/{being}/image', 'BeingController@imageUpload');
Route::post('/atlas/weapon/{weapon}/image', 'WeaponController@imageUpload');
Route::post('/atlas/object/{object}/image', 'ObjectController@imageUpload');
Route::post('/atlas/event/{event}/image', 'EventController@imageUpload');
Route::post('/atlas/species/{species}/image', 'EventController@imageUpload');
Route::post('/atlas/language/{language}/image', 'EventController@imageUpload');
Route::post('/atlas/law/{law}/image', 'EventController@imageUpload');
Route::post('/atlas/organization/{organization}/image', 'OrganizationController@imageUpload');
Route::post('/atlas/currency/{currency}/image', 'CurrencyController@imageUpload');

## Admin routes

Route::middleware(['auth', 'admin'])->group(function () {
  Route::get('/admin', 'AdminController@index');
  Route::get('/admin/users', 'AdminController@userIndex');
  Route::get('/admin/users/create', 'AdminController@createUser');
  Route::get('/admin/users/{user}', 'AdminController@userShow');
  Route::post('/admin/users', 'AdminController@userStore');
  Route::get('/admin/users/{user}/edit', 'AdminController@userEdit');
  Route::get('/admin/app/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
  Route::delete('/admin/user/{user}', 'AdminController@userDestroy');
  Route::patch('/admin/users/{user}/adminToggle', 'AdminController@adminToggle');
  Route::patch('/admin/users/{user}', 'AdminController@updateUser');
  Route::patch('/admin/users/{user}/modToggle', 'AdminController@modToggle');
  Route::patch('/admin/users/{user}/trustToggle', 'AdminController@trustToggle');
  Route::patch('/admin/users/{user}/banToggle', 'AdminController@banToggle');
});

## Moderator routes

Route::middleware(['auth', 'moderator'])->group(function () {
  Route::get('/moderator', 'ModeratorController@index');
  Route::get('/moderator/users', 'ModeratorController@userIndex');
  Route::get('/moderator/users/create', 'ModeratorController@createUser');
  Route::get('/moderator/users/{user}', 'ModeratorController@userShow');
  Route::post('/moderator/users', 'ModeratorController@userStore');
  Route::get('/moderator/users/{user}/edit', 'ModeratorController@userEdit');
  Route::get('/moderator/app/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
  Route::patch('/moderator/users/{user}', 'ModeratorController@updateUser');
  Route::patch('/moderator/users/{user}/modToggle', 'ModeratorController@modToggle');
  Route::get('/moderator/queues', 'ModQueueController@index');
  Route::get('/moderator/queues/moderation', 'ModQueueController@moderationQueueIndex');
});

/*
  Errors
*/
Route::view('/403', '403');
Route::view('/404', '404');

Auth::routes();
Route::post('login', 'AuthController@authenticate')->name('login');
Route::get('banned', 'HomeController@banned')->name('banned');

// Route::get('/home', 'HomeController@index')->name('home');
